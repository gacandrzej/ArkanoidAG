
package arkanoidag;

import java.awt.Point;
/**
 * @author Andrzej Gac
 * last update Toruń 17-03-2021 ZSMEiE
 */
public class Cegła {
    public Point pos;
    public int szerCegły;
    public int wysCegły;
    public boolean czyZbita;
    public Cegła(int sze, int wys, Point _pos) {
     szerCegły = sze;
     wysCegły = wys;
     pos = _pos;
    }
    public Cegła(int sze, int wys, Point _pos,boolean _czyZbita) {
     szerCegły = sze;
     wysCegły = wys;
     pos = _pos;
     czyZbita= _czyZbita;
    }
}
