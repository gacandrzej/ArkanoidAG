package arkanoidag;

/**
 * @author Andrzej Gac
 * last update Toruń 17-03-2021 ZSMEiE
 */
public class Config {
    public static int SZE_OKNA=1027;
     public static int WYS_OKNA=650;
     public static int SZE_CEGŁY=100;
     public static int WYS_CEGŁY=30;
     public static int ODSTEP_CEGŁY=3;
     public static final int TX=SZE_OKNA/SZE_CEGŁY;
     public static final int TY=WYS_OKNA/2/WYS_CEGŁY;
     public static final int PALETKA_MAX_SPEED = 5;
    public Config() {
    }
     
}