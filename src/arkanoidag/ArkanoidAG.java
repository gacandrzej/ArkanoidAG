package arkanoidag;

import java.awt.Color;
import java.awt.HeadlessException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author Andrzej Gac
 * last update Toruń 18-03-2021 ZSMEiE
 */
public class ArkanoidAG extends JFrame{
   
    static JLabel wynik_punkty=null;
    JLabel wynik=null;
    JButton start = new JButton("START");
    PanelGry panel = new PanelGry();

    public ArkanoidAG() throws HeadlessException {
        setTitle("-Arkanoid-wersja 2.0 --");
        setSize(Config.SZE_OKNA+200, Config.WYS_OKNA+20);
        setAlwaysOnTop(true);
        setLayout(null);
        panel.setBounds(0, 0, Config.SZE_OKNA, Config.WYS_OKNA);
        add(panel);
        this.getContentPane().setBackground(new Color(17,70,128));
        setResizable(false);
        //Insets margines = new Insets(1,1,10,10);
        start.setBounds(Config.SZE_OKNA+10, 110, 80, 30); // położenie, wymiary
       // start.setMargin(margines);
        add(start);
        wynik = new JLabel("Wynik:");
        wynik.setBounds(Config.SZE_OKNA+10, 230, 80, 30);
        add(wynik);
        wynik_punkty = new JLabel("0");
        wynik_punkty.setBounds(Config.SZE_OKNA+100, 230, 80, 30);
        add(wynik_punkty);
        //----------------------------
        setDefaultCloseOperation(3);
        setVisible(true);
    }

   
    public static void main(String[] args) {
        ArkanoidAG o =new ArkanoidAG();
    }
    
}
