package arkanoidag;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * @author Andrzej Gac
 * last update Toruń 29-03-2021 ZSMEiE
 */
public class PanelGry extends JPanel implements ActionListener, KeyListener {
    
    private int vx = -2;
    private int vy = -2;
    int X = Config.TX;
    int Y = Config.TY;
    Cegła cegły[][] = new Cegła[X][Y];
    Timer timer;
    Paletka paletka = new Paletka(Paletka.szerPaletki, Paletka.wysPaletki, new Point((Config.SZE_OKNA-Paletka.szerPaletki )/ 2, Config.WYS_OKNA-Paletka.wysPaletki));
    Pilka pilka= new Pilka(Pilka.srednica, new Point(300,500));
    /** 
     * StaGry to zmienna ... ma wartośći WIN LOST DURANCE
     */
     private String stanGry = "DURANCE"; 
     public int iloscZbitychCegieł = 0;
    private int delay = 10;
    public PanelGry() {
        setBackground(new Color(50, 50, 30));
        setBounds(0, 0, Config.SZE_OKNA, Config.WYS_OKNA);
        
        Map(X-1,Y);
    
        timer = new Timer(10, this);
        timer.start();
        
        //addKeyListener(new Sterowanie());
         addKeyListener(new ArkanoidListener());
        setFocusable(true);
    }

private void Map(int row, int col){
    for (int i = 1; i < row; i++) {
            for (int j = 0; j < col; j++) {
                cegły[i][j] = new Cegła(Config.SZE_CEGŁY, Config.WYS_CEGŁY, new Point(i * (Config.SZE_CEGŁY+Config.ODSTEP_CEGŁY), j * (Config.WYS_CEGŁY+Config.ODSTEP_CEGŁY)),false);
                
            }
        }
}
    private void rysujCegly(Graphics2D g) {

        for (int i = 1; i < X-1; i++) {
            for (int j = 0; j < Y; j++) {
                if(cegły[i][j].czyZbita == false){
                    g.setColor(Color.red);
                    g.fillRect(cegły[i][j].pos.x, cegły[i][j].pos.y, Config.SZE_CEGŁY, Config.WYS_CEGŁY);
                    //
                // odstępy dla cegieł ?
                //
                //g.setStroke(new BasicStroke(3));
                
                    g.setColor(Color.BLACK);
                    g.drawRect(cegły[i][j].pos.x, cegły[i][j].pos.y, Config.SZE_CEGŁY, Config.WYS_CEGŁY);  
                }
                

            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ArkanoidAG.wynik_punkty.setText(String.valueOf(iloscZbitychCegieł));
        if (stanGry.equals("DURANCE")) {
            rysujCegly((Graphics2D)g);
            rysujPaletke(g);
            rysujPilke(g);
        }
        else { 
            endGame(g);
        }
        g.dispose();
    }
    private void endGame(Graphics g) {
        Font czcionka = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metr = getFontMetrics(czcionka);
        g.setColor(Color.white);
        g.setFont(czcionka);
        g.drawString(stanGry+" "+iloscZbitychCegieł+" Press Enter to restart level", (Config.SZE_OKNA - metr.stringWidth(stanGry)) / 2, Config.WYS_OKNA / 2);
    }
    
     private void checkGame(){
        if (pilka.pos.y > Config.WYS_OKNA)
        {
            stanGry = "LOST";
        }
        
        if (iloscZbitychCegieł == (X-2)*Y)
        {
            stanGry = "WIN";
        }
    }
     
     void rysujPilke(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillOval(pilka.pos.x, pilka.pos.y, pilka.srednica, pilka.srednica);
        g.setColor(Color.yellow);
        g.drawOval(pilka.pos.x, pilka.pos.y, pilka.srednica, pilka.srednica);
        g.setColor(Color.GREEN);
        
    }
     
    void rysujPaletke(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(paletka.pos.x, paletka.pos.y, Paletka.szerPaletki, Paletka.wysPaletki);      
        g.setColor(Color.BLACK);
    }
    void ruchPilki(){
       if (pilka.pos.x<=0 || pilka.pos.x > Config.SZE_OKNA-pilka.srednica) {
           vx = -vx;
       }
       if (pilka.pos.y<= 0  || pilka.pos.y + pilka.srednica >= (Config.WYS_OKNA-Paletka.wysPaletki) 
            && pilka.pos.x > paletka.pos.x &&  pilka.pos.x < paletka.pos.x +Paletka.szerPaletki
               ) {
            vy = -vy;
        }
        pilka.pos.x += vx;
        pilka.pos.y += vy;
    }
    private void zderzenieZCegla(){
        //
        // lepiej Rectangle.intersects
        //
        if(stanGry=="DURANCE"){
            if (new Rectangle(pilka.pos.x,pilka.pos.y,20,20).intersects(new Rectangle(paletka.pos.x, paletka.pos.y, Paletka.szerPaletki, Paletka.wysPaletki))){
                vy = -vy;
              
            }
        }
        //
        // stary kod
        //
        for (int i = 1; i<X-1; i++){
            for (int j=0; j<Y;j++){
                if(cegły[i][j].czyZbita == false){ 
                    if(pilka.pos.x > cegły[i][j].pos.x && pilka.pos.x < cegły[i][j].pos.x + Config.SZE_CEGŁY && pilka.pos.y + pilka.srednica > cegły[i][j].pos.y && pilka.pos.y < cegły[i][j].pos.y+ Config.WYS_CEGŁY){        // uderzenie góra dół   
                        vy = -vy;
                        cegły[i][j].czyZbita = true;
                        iloscZbitychCegieł++;
                    }else if(pilka.pos.y > cegły[i][j].pos.y && pilka.pos.y < cegły[i][j].pos.y + Config.WYS_CEGŁY && pilka.pos.x + pilka.srednica > cegły[i][j].pos.x&& pilka.pos.x < cegły[i][j].pos.x + Config.SZE_CEGŁY){     // uderzenie prawo lewo
                    vx = -vx;
                    cegły[i][j].czyZbita = true;
                     iloscZbitychCegieł++;
                }
                }
            }
        }
    }
   

    

    public class Sterowanie extends KeyAdapter {

        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            if (key == KeyEvent.VK_LEFT && paletka.pos.x > 0) {
                paletka.pos.x -= 20;
            } else if (key == KeyEvent.VK_RIGHT && paletka.pos.x < Config.SZE_OKNA - Paletka.szerPaletki) {
                paletka.pos.x += 20;
            }
        }
    }
public class ArkanoidListener implements KeyListener
    {
        @Override
        public void keyTyped(KeyEvent e) {}

        @Override
        public void keyPressed(KeyEvent e)
        {
            if(e.getKeyCode() == e.VK_LEFT)
            {
                paletka.speed.x = -Config.PALETKA_MAX_SPEED;
            }
            if(e.getKeyCode() == e.VK_RIGHT)
            {
                paletka.speed.x = Config.PALETKA_MAX_SPEED;
            }
            if (e.getKeyCode()== KeyEvent.VK_ENTER){
                if (stanGry=="WIN"|| stanGry=="LOST"){
                    stanGry ="DURANCE";
                    iloscZbitychCegieł = 0;
                    vx = -2;
                    vy = -2;
                    pilka.pos.x=300;
                    pilka.pos.y=500;
                    Map(X-1,Y);
                    repaint();
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e)
        {
            paletka.speed.x = 0;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        
        checkGame();
        zderzenieZCegla();
        ruchPilki();
        paletka.Update();
        repaint();
    }
     @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
