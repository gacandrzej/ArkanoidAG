package arkanoidag;

import java.awt.Point;
/**
 * @author Andrzej Gac
 * last update Toruń 17-03-2021 ZSMEiE
 */
public class Paletka {
    public Point pos = null;
    public Point speed = null;
     // public Point pos;
      public static int szerPaletki = 100;
      public static int wysPaletki = 30;
      public static int speedPaletki = 10;
    public Paletka(int sze,int wys, Point _pos ) {
        szerPaletki=sze;
        wysPaletki=wys;
        pos=_pos;
        speed = new Point(0,0);
    }

     public void Move(int x, int y)
    {
        pos.move(pos.x + x, pos.y + y);
    }

    public void Update()
    {
        Move(speed.x, speed.y);
        Edges();
    }

    public void Edges()
    {
        if(pos.x < 0)
        {
            pos.x = 0;
        }
        else if(pos.x + szerPaletki> Config.SZE_OKNA)
        {
            pos.x = Config.SZE_OKNA- szerPaletki;
        }
    }
}